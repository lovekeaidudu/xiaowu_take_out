package com.xiaowu.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiaowu.reggie.entity.ShoppingCart;

public interface ShoppingCartService extends IService<ShoppingCart> {

}
