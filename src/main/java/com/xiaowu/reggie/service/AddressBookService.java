package com.xiaowu.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiaowu.reggie.entity.AddressBook;

public interface AddressBookService extends IService<AddressBook> {

}
