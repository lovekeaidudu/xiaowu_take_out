package com.xiaowu.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiaowu.reggie.common.BaseContext;
import com.xiaowu.reggie.common.R;
import com.xiaowu.reggie.dto.OrdersDto;
import com.xiaowu.reggie.entity.OrderDetail;
import com.xiaowu.reggie.entity.Orders;
import com.xiaowu.reggie.service.OrderDetailService;
import com.xiaowu.reggie.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 订单
 */
@Slf4j
@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @Autowired
    private OrderDetailService orderDetailService;

    /**
     * 用户下单
     * @param orders
     * @return
     */
    @PostMapping("/submit")
    public R<String> submit(@RequestBody Orders orders){
        log.info("订单数据：{}",orders);
        orderService.submit(orders);
        return R.success("下单成功");
    }

    /**
     * 获取历史订单分页
     * @param page
     * @param pageSize
     * @return
     */
    @GetMapping("/userPage")
    public R<Page> getOrders(int page, int pageSize){
        Page<Orders> pageInfo = new Page<>(page, pageSize);
        Page<OrdersDto> dtoPage = new Page<>();
        Long currentId = BaseContext.getCurrentId();
        LambdaQueryWrapper<Orders> queryWrapper = new LambdaQueryWrapper<>();
        // 根据用户id分页查询出该用户的订单信息
        queryWrapper.eq(currentId != null, Orders::getUserId, currentId)
                .orderByDesc(Orders::getOrderTime);
        orderService.page(pageInfo, queryWrapper);
        // 复制page
        BeanUtils.copyProperties(pageInfo, dtoPage, "records");
        // 将查出来的订单信息 根据orderid查出订单对应的菜品信息 从新转成list
        List<OrdersDto> ordersDtos = pageInfo.getRecords().stream().map((item) -> {
            OrdersDto dto = new OrdersDto();
            BeanUtils.copyProperties(item, dto);
            LambdaQueryWrapper<OrderDetail> lambdaQueryWrapper = new LambdaQueryWrapper<>();
            lambdaQueryWrapper.eq(item.getId() != null, OrderDetail::getOrderId, item.getId());
            List<OrderDetail> orderDetails = orderDetailService.list(lambdaQueryWrapper);
            dto.setOrderDetails(orderDetails);
            return dto;
        }).collect(Collectors.toList());
        // 从新返回结果集
        dtoPage.setRecords(ordersDtos);
        return R.success(dtoPage);
    }

    /**
     * 管理系统订单分页
     * @param page
     * @param pageSize
     * @param number
     * @return
     */
    @GetMapping("/page")
    public R<Page> getOrders(int page, int pageSize, String number, String beginTime, String endTime){
        Page<Orders> pageInfo = new Page<>(page, pageSize);
        LambdaQueryWrapper<Orders> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(number != null, Orders::getNumber, number)
                .between(beginTime != null || endTime != null,Orders::getOrderTime, beginTime, endTime)
                .orderByDesc(Orders::getOrderTime);
        orderService.page(pageInfo, queryWrapper);

        return R.success(pageInfo);
    }
}