package com.xiaowu.reggie.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.xiaowu.reggie.entity.User;
import com.xiaowu.reggie.mapper.UserMapper;
import com.xiaowu.reggie.service.UserService;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
}
