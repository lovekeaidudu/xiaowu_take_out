package com.xiaowu.reggie.dto;

import com.xiaowu.reggie.entity.OrderDetail;
import com.xiaowu.reggie.entity.Orders;
import lombok.Data;

import java.util.List;

@Data
public class OrdersDto extends Orders {

    // orderDetails
    private List<OrderDetail> orderDetails;
}
