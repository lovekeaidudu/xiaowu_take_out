package com.xiaowu.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiaowu.reggie.entity.DishFlavor;

public interface DishFlavorService extends IService<DishFlavor> {
}
