package com.xiaowu.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiaowu.reggie.entity.Category;


public interface CategoryService extends IService<Category> {
    public void remove(Long id);
}
