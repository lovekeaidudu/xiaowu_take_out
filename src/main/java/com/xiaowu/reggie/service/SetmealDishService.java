package com.xiaowu.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiaowu.reggie.entity.SetmealDish;

public interface SetmealDishService extends IService<SetmealDish> {
}
