package com.xiaowu.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiaowu.reggie.entity.OrderDetail;

public interface OrderDetailService extends IService<OrderDetail> {

}
