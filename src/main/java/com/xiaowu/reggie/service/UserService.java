package com.xiaowu.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiaowu.reggie.entity.User;

public interface UserService extends IService<User> {
}
